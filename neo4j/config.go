package neo4j

// Config set of neo4j configurations
type Config struct {
	URI      string `required:"true"`
	User     string `required:"true"`
	Password string `required:"true"`
}
