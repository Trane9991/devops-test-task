package neo4j

import (
	"errors"
	"fmt"
	"log"

	"github.com/neo4j/neo4j-go-driver/v4/neo4j"
)

var driver neo4j.Driver

type Item struct {
	ID   int64
	Name string
}

// Init initializes Neo4j connection
func Init(conf *Config) error {
	var err error
	driver, err = neo4j.NewDriver(conf.URI, neo4j.BasicAuth(conf.User, conf.Password, ""))
	if err != nil {
		return fmt.Errorf("failed to create neo4j driver: %w", err)
	}

	if err := driver.VerifyConnectivity(); err != nil {
		return fmt.Errorf("couldn't verify neo4j connection: %w", err)
	}

	if err := checkMigration(); err != nil {
		return fmt.Errorf("migration check failed: %w", err)
	}

	return nil
}

// Shutdown terminates Neo4j connection
func Shutdown() {
	if err := driver.Close(); err != nil {
		log.Println("failed to close driver:", err)
	}
}

func CreateItem(item *Item) error {
	query := "CREATE (n:Item { id: $id, name: $name }) RETURN n.id, n.name"
	params := map[string]interface{}{"id": item.ID, "name": item.Name}

	session := driver.NewSession(neo4j.SessionConfig{})
	defer session.Close()
	result, err := session.Run(query, params)
	if err != nil {
		return err
	}

	var record *neo4j.Record
	for result.NextRecord(&record) {
		props := make(map[string]interface{}, len(result.Record().Keys))
		for i, k := range result.Record().Keys {
			props[k] = record.Values[i]
		}
		log.Println("Created item with properties", props)
	}

	sum, err := result.Consume()
	if err != nil {
		return err
	}
	log.Println("Successfully created", sum.Counters().NodesCreated(), "nodes")

	return result.Err()
}

func GetItem(item *Item) ([]*Item, error) {
	query := "MATCH (n:Item) %s RETURN n;"
	condition := ""
	parameters := make(map[string]interface{})

	if item != nil {
		condition = "WHERE n.id = $id"
		parameters["id"] = item.ID
		if item.Name != "" {
			parameters["name"] = item.Name
			condition += " and n.name = $name"
		}

	}
	query = fmt.Sprintf(query, condition)

	session := driver.NewSession(neo4j.SessionConfig{})
	defer session.Close()
	result, err := session.Run(query, parameters)
	if err != nil {
		return nil, err
	}

	var items = []*Item{}
	var record *neo4j.Record
	for result.NextRecord(&record) {
		n := record.Values[0].(neo4j.Node)
		items = append(items, &Item{ID: n.Props["id"].(int64), Name: n.Props["name"].(string)})
	}

	return items, result.Err()
}

func checkMigration() error {
	query := "MATCH (m:Migration) RETURN m"

	session := driver.NewSession(neo4j.SessionConfig{})
	defer session.Close()
	result, err := session.Run(query, nil)
	if err != nil {
		return err
	}

	var isMigrated bool
	var record *neo4j.Record
	for result.NextRecord(&record) {
		isMigrated = true
	}

	if !isMigrated {
		return errors.New("required migrations were not run")
	}

	return nil
}
