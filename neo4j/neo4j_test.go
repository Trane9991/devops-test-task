package neo4j

import (
	"os"
	"reflect"
	"testing"
)

func TestMain(m *testing.M) {
	Init(&Config{
		URI:      "neo4j://neo4j.example.com",
		User:     "neo4j",
		Password: "1111111",
	})

	os.Exit(m.Run())
}

func TestNeo4jQueries(t *testing.T) {
	var testcases = []struct {
		Name  string
		Items []*Item
	}{
		{
			Name: "Create multiple items",
			Items: []*Item{
				{123, "123"},
				{333, "333"},
				{777, "777"},
			},
		},
		{
			Name:  "Create zero items",
			Items: []*Item{},
		},
		{
			Name:  "Nil items",
			Items: nil,
		},
	}

	for _, test := range testcases {
		t.Run(test.Name, func(t *testing.T) {

			// create set of items
			for _, item := range test.Items {
				if err := CreateItem(item); err != nil {
					t.Error(err)
				}
			}

			for _, item := range test.Items {
				items, err := GetItem(item)
				if err != nil {
					t.Error(err)
				}
				if len(items) == 0 {
					t.Error("expected at least one item")
				}
				if !reflect.DeepEqual(item, items[0]) {
					t.Error("items doesn't match")
				}
			}
		})
	}

}
