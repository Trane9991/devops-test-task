# Welcome to the GetSocial test task

Here you can find small [Golang](https://golang.org/) application which uses [Neo4j](https://neo4j.com/) as database.

Unfortunately previous developers left this project unfinished, your job is to
figure out what is going on here, how to run it, document and prepare it for future team members to develop new features.

## What to do

1. Fork this repo

2. Check the **[Issues](https://bitbucket.org/getsocial/devops-test-task/issues?status=new&status=open)** for the issues, tasks and clues. Pay attention, to the issue priorities, the ones with the low priority are nice to have, but can be skipped if you are short on time.

3. Have fun
